﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that allows the player to move where he has chosen to.
/// </summary>
public class AITesting : MonoBehaviour
{
    //--------------------------------------------
    // Public Variables
    //--------------------------------------------
    /// <summary>
    /// Destination 
    /// </summary>
    public Transform destination;

    /// <summary>
    /// Movement Speed of the player
    /// </summary>
    public float speedo;

    /// <summary>
    /// Flag that tells if the player has arrived its destination
    /// </summary>
    public bool hasArrived;

    //--------------------------------------------
    // Methods
    //--------------------------------------------

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(moveToTarget());
	}

    IEnumerator moveToTarget()
    {
        while(hasArrived == false)
        {
            Vector3 destinyPosition = destination.position;
            destinyPosition.y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, destinyPosition, Time.deltaTime * speedo);
            if(transform.position == destinyPosition)
            {
                hasArrived = true;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

	
	// Update is called once per frame
	void Update ()
    {
        
	}
}
