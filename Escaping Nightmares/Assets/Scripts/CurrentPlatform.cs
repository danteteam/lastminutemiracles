﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used to define the different platforms where the game can run
/// </summary>
public class CurrentPlatform : MonoBehaviour
{
    public enum CURRENT_PLATFORM { PC, OCULUS_TOUCH, GEAR_VR, HTC_VIVE};

    public CURRENT_PLATFORM platform;
}
