﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used to end the game when the team of players finish the game
/// </summary>
public class WinnersAnnouncement : MonoBehaviour
{
    //----------------------------------------------
    // Public Variables
    //---------------------------------------------
    /// <summary>
    /// Lights used to show the players that they won
    /// </summary>
    public GameObject[] awesomeLights;

    /// <summary>
    /// Time the players spent completing the game
    /// </summary>
    public float totalTime;

    /// <summary>
    /// Start time of the session
    /// </summary>
    private float startTime;

    /// <summary>
    /// Time when the players finished the game
    /// </summary>
    private float endtime;

    //----------------------------------------------
    // Methods
    //---------------------------------------------

    // Use this for initialization
    void Start ()
    {
        startTime = Time.time;
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "HTCPlayer" || other.tag == "GearPlayer")
        {
            endtime = Time.time;

            totalTime = endtime - startTime;
            
            foreach(GameObject temp in awesomeLights)
            {
                temp.SetActive(true);
            }
        }
    }
}
