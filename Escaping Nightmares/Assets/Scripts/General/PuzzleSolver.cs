﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used in the pedestals to check if the key the player is putting in correspond to the same color
/// </summary>
public class PuzzleSolver : Photon.PunBehaviour, IPunObservable
{
    //-----------------------------------------
    // Public Variables
    //-----------------------------------------
    /// <summary>
    /// Expected Key to be placed above this pedestal
    /// </summary>
    public GameObject expectedObject;

    /// <summary>
    /// Associated door to this pedestal
    /// </summary>
    public DoorMechanism associatedDoor;

    /// <summary>
    /// Area Collider used to know if a player has entered it's trigger area to put a key
    /// </summary>
    public SphereCollider areaCollider;

    /// <summary>
    /// Spotlight used when the key places is the correct one
    /// </summary>
    public GameObject spotlight;

    //-----------------------------------------
    // Public Variables
    //-----------------------------------------
    
    /// <summary>
    /// Checks the received key to see if it matches the expected one. If it does then returns true
    /// </summary>
    /// <param name="receivedObject"></param>
    /// <returns></returns>
    public bool checkPuzzle(GameObject receivedObject)
    {
        bool answer = false;
        if (expectedObject == receivedObject)
        {
            answer = true;
        }
        return answer;
    }

    /// <summary>
    /// Solves the puzzle and notifies the clients that this puzzle has been solved
    /// </summary>
    [PunRPC]
    public void solvePuzzle()
    {
        associatedDoor.increaseSolvedPuzzles();
        this.enabled = false;
        areaCollider.enabled = false;
        spotlight.SetActive(true);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
