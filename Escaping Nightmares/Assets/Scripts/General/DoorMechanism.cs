﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that handles the door opening mechanism
/// </summary>
public class DoorMechanism : Photon.PunBehaviour, IPunObservable
{
    //-------------------------------------------------
    // Public Variables
    //-------------------------------------------------

    /// <summary>
    /// Number of puzzles required to be solved before opening this door.
    /// </summary>
    public int totalPuzzles;

    /// <summary>
    /// Animator of the door
    /// </summary>
    public Animator doorAnimator;

    /// <summary>
    /// Number of puzzles required to be solved before opening this door.
    /// </summary>
    private int expectedPuzzlesSolved;

    /// <summary>
    /// Number of puzzles solved by the players
    /// </summary>
    private int solvedPuzzles;

    //-------------------------------------------------
    // Public Variables
    //-------------------------------------------------

    // Use this for initialization
    void Start()
    {
        expectedPuzzlesSolved = totalPuzzles;
        solvedPuzzles = 0;
    }

    /// <summary>
    /// Checks if all the expected puzzles have been solved. If they are, proceeds to make a RPC to notify the clients to open the door
    /// </summary>
    [PunRPC]
    public void checkIfSolved()
    {
        if (solvedPuzzles == expectedPuzzlesSolved)
        {
            doorAnimator.SetTrigger("OpenDoor");
        }
    }

    /// <summary>
    /// When the players have solve a puzzle, increase the number of puzzles solved and tells the clients to check if all the puzzles have been solved
    /// </summary>
    public void increaseSolvedPuzzles()
    {
        solvedPuzzles++;
        if(PhotonNetwork.connectionState == ConnectionState.Connected && photonView != null)
        {
            photonView.RPC("checkIfSolved", PhotonTargets.All);
        }
        else
        {
            checkIfSolved();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
