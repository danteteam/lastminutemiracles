﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the information used in the mural tutorials. Depending of the device of the player, the mural displays different information on how to use the controls
/// </summary>
public class MenuUIHandler : MonoBehaviour
{
    //----------------------------------------------------
    // Public Variables
    //----------------------------------------------------
    /// <summary>
    /// Class that manages the network connections
    /// </summary>
    public NetworkManager networkManager;

    // 0: PC, 1: OculusTouch, 2: GearVR, 3: HTCVive
    public GameObject[] platformCanvases;

    //----------------------------------------------------
    // Methods
    //----------------------------------------------------
    /// <summary>
    /// Sets the canvas with the tutorial information according to the device the player is using
    /// </summary>
    /// <param name="currentPlatform"></param>
    public void setCanvas(LvlManager.Platform currentPlatform)
    {
        int thePlatform = (int)currentPlatform;
        for (int i = 0; i < platformCanvases.Length; i++)
        {
            GameObject temp = platformCanvases[i];
            if (i == thePlatform)
            {
                temp.SetActive(true);
            }
            else
            {
                temp.SetActive(false);
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        setCanvas(networkManager.platform);
    }
}
