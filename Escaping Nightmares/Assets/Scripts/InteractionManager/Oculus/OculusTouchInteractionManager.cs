﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Manager for the Oculus Players Interactions
/// </summary>
public class OculusTouchInteractionManager : Photon.PunBehaviour
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the oculus inputs
    public OculusInputManager oculusInputManager;

    //---------------------------------------
    // Switchable Interactions Variables
    //---------------------------------------

    // Script that handles the teleportation process for the right arm
    public PlayerMotion motionManager;

    // Script that handles the interaction process for the left arm
    public PlayerInteraction playerInteractionManager;

    //---------------------------------------
    // Private Variables
    //---------------------------------------
    // Has the player taken an item already?
    private bool hasTakenItem;

    // Is the player in navigation mode and aiming right now?
    private bool isAiming;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start ()
    {
        GameObject oculusTouchCamera = GameObject.Find("OculusTouchCamera");
        if (oculusTouchCamera != null)
        {
            oculusInputManager = oculusTouchCamera.GetComponent<OculusInputManager>();
            oculusInputManager.OnCameraMoveEvent += OnCameraMoved;
            oculusInputManager.OnRightGabbrerBegin += OnRightGrabber;
            oculusInputManager.OnLeftGrabberBegin += OnLeftGrabber;
            oculusInputManager.OnRightTriggerBegin += OnRightTriggerPressed;
        }
    }

    /// <summary>
    /// The player has used the right grabber, used to activate the closed fist animation, also enables the navigation mode
    /// </summary>
    /// <param name="isGrabbing"></param>
    public void OnRightGrabber(bool isGrabbing)
    {
        motionManager.animationManager.setBoolAnim(true, 1, "ClosedHand", isGrabbing);
        motionManager.SetMarkerStatus(isGrabbing);
        isAiming = isGrabbing;
    }

    /// <summary>
    /// The player closes his left hand and takes an item if it has one near
    /// </summary>
    /// <param name="isGrabbing"></param>
    public void OnLeftGrabber(bool isGrabbing)
    {
        playerInteractionManager.TakeItem(isGrabbing);
        motionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", isGrabbing);
    }

    /// <summary>
    /// If the player is aiming and presses the right trigger button, executes the movement action
    /// </summary>
    public void OnRightTriggerPressed()
    {
        if(isAiming == true)
        {
            motionManager.MotionAction();
        }
    }

    /// <summary>
    /// Method used to update the camera position 
    /// </summary>
    /// <param name="playerCamera">The Camera Position</param>
    public void OnCameraMoved(Transform playerCamera)
    {
        motionManager.MoveCamera(playerCamera);
    }
}
