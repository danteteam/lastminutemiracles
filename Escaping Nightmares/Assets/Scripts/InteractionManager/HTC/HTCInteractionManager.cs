﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Interaction Manager for the HTC Vive Users. This class allows the player to move with his right hand 
/// by pointing out a place in the floor to where he wants to move and take/drop an item with his left hand
/// </summary>
public class HTCInteractionManager : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the htc Vive inputs
    public HTCInputManager htcInputManager;

    //---------------------------------------
    // Switchable Interactions Variables
    //---------------------------------------

    // Script that handles the teleportation process for the right arm
    public PlayerMotion playerMotionManager;

    // Script that handles the interaction process for the left arm
    public PlayerInteraction playerInteractionManager;

    //---------------------------------------
    // Private Variables
    //---------------------------------------
    // Is the player aiming at something while navigation mode is on?
    public bool isAiming;

    // The HTC Player has a virtual pad where he stands on. He can only aim and move when he is on the pad.
    // This works as a restriction to make the navigation equivalent with the Gear VR user.
    public bool isOnPad;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start()
    {
        string[] nameSplit = transform.parent.gameObject.name.Split(' ');
        string number = nameSplit[1];
        GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject temp in array)
        {
            if (temp.name.Contains("Camera") && temp.name.Contains(number))
            {
                htcInputManager = temp.GetComponent<HTCInputManager>();
                break;
            }
        }        

        if (htcInputManager != null)
        {
            htcInputManager.OnCameraMoveEvent += OnCameraMoved;
            htcInputManager.OnLeftTriggerEvent += OnLeftGrabber;
            htcInputManager.OnRightGrabberEvent += OnRightGrabber;
            htcInputManager.OnRightTriggerEvent += OnRightTriggerPressed;
        }
    }

    /// <summary>
    /// Method used to update the camera position 
    /// </summary>
    /// <param name="playerCamera">The Camera Position</param>
    public void OnCameraMoved(Transform playerCamera)
    {
        playerMotionManager.MoveCamera(playerCamera);
    }

    /// <summary>
    /// Activates the navigation mode on the HTC Vive
    /// </summary>
    /// <param name="isGrabbing"></param>
    public void OnRightGrabber(bool isGrabbing)
    {
        playerMotionManager.animationManager.setBoolAnim(true, 1, "ClosedHand", isGrabbing);
        if(playerMotionManager.busyToWalk == false)
        {
            if (isOnPad == true)
            {
                isAiming = isGrabbing;
                playerMotionManager.SetMarkerStatus(isGrabbing);
            }
            else if (isOnPad == false)
            {
                isAiming = false;
                playerMotionManager.SetMarkerStatus(false);
            }
        }        
    }

    /// <summary>
    /// Takes the item and executes the animation of closing the hand
    /// </summary>
    /// <param name="isGrabbing"></param>
    public void OnLeftGrabber(bool isGrabbing)
    {
        playerInteractionManager.TakeItem(isGrabbing);
        playerMotionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", isGrabbing);
    }

    /// <summary>
    /// If is aiming(navigation mode on) and presses the right trigger, moves the player towards the destination
    /// </summary>
    public void OnRightTriggerPressed()
    {
        if(isAiming == true)
        {
            playerMotionManager.MotionAction();
        }
    }

    /// <summary>
    /// Tells if the player is on a color platform
    /// </summary>
    /// <param name="other">Game Object that has entered in contact with the trigger of the player</param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "HTCPlatform")
        {
            isOnPad = true;
        }
    }

    /// <summary>
    /// Tells if the player has left a color platform
    /// </summary>
    /// <param name="other">Game Object that has left contact with the trigger of the player</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "HTCPlatform")
        {
            isOnPad = false;
            playerMotionManager.SetMarkerStatus(false);
        }
    }

    // This method is not required for this project but it is required by Photon
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
