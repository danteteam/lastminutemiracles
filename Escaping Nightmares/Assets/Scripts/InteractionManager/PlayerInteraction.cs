﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the player interactions. The player can only take items that are of the same color
/// And if he doesn't have already on his hand an item. The items are keys used to open doors in the game.
/// You can only grab them with your left hand or when the interaction mode is activated in the Gear.
/// </summary>
public class PlayerInteraction : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // ID of the actual player
    public int playerID;

    // The local player instance. Use this to know if the local player is represented in the Scene
    public static GameObject LocalPlayerInstance;

    // Current Player Color
    public PlayerColor currentPlayerColor;

    // Transform position of the hand of the avatar. If the player takes an item, then the item will be placed as a child of this transform.
    public Transform grabPosition;

    //---------------------------------------
    // Private Variables
    //---------------------------------------

    // This is GameObject is null when the player is not near to any item. It stops being null when the player comes close to an item.
    protected GameObject possibleItem;

    // This is not null when the player has come close to a pedestal.
    protected GameObject possiblePedestalToDropPlace;

    // Does the player have an item in his left hand right now?
    protected bool hasTakenItem;

    // Locks the Grabber so there can only be one grab action
    private bool grabberLock;

    //---------------------------------------
    // Methods
    //---------------------------------------

    /// <summary>
    /// Assigns a possible item or an altar when a player comes close to one of those in-game
    /// </summary>
    /// <param name="objectColliding"></param>
    void OnTriggerEnter(Collider objectColliding)
    {
        //Debug.Log("Colliding with: " + objectColliding.gameObject.tag);
        if (objectColliding.gameObject.tag == "InteractuableObject")
        {
            //cameraUIHandler.setCanInteract(true);
            possibleItem = objectColliding.gameObject;
        }
        else if (objectColliding.gameObject.tag == "DropoffArea")
        {
            possiblePedestalToDropPlace = objectColliding.gameObject;
        }
    }

    /// <summary>
    /// Deassigns a possible item or an altar when a player leaves its trigger area
    /// </summary>
    /// <param name="objectColliding"></param>
    void OnTriggerExit(Collider objectColliding)
    {
        //Debug.Log("Exiting with: " + objectColliding.gameObject.tag);
        if (objectColliding.gameObject.tag == "InteractuableObject")
        {
            //cameraUIHandler.setCanInteract(false);
            possibleItem = null;
        }
        else if (objectColliding.gameObject.tag == "DropoffArea")
        {
            possiblePedestalToDropPlace = null;
        }
    }

    /// <summary>
    /// Method that takes an item if the player is close to it and is of the same color.
    /// </summary>
    /// <param name="grabberHeld"></param>
    public void TakeItem(bool grabberHeld)
    {
        if(grabberLock != grabberHeld)
        {
            grabberLock = grabberHeld;
            // Checks if the player is in an area where there is an interactuable object
            if (possibleItem != null)
            {
                // If the player doesn't already have an item in his hand then attemps to grab the possible item
                if (hasTakenItem == false)
                {
                    InteractiveItem item = possibleItem.GetComponent<InteractiveItem>();
                    // If the possible item is of the same color, then the player can take it
                    if (item.currentColor == currentPlayerColor.currentColor)
                    {
                        // Validates that the player is using the grab action and he doesn't have any item on his hand already
                        if (grabberHeld == true && hasTakenItem == false)
                        {
                            hasTakenItem = true;
                            if (PhotonNetwork.inRoom)
                            {
                                if (PhotonNetwork.connectionState == ConnectionState.Connected && photonView != null)
                                {
                                    // When the player takes the item, executes the RPC to take the item and notify the other players of that event.
                                    photonView.RPC("PUNTakeItem", PhotonTargets.All, hasTakenItem);
                                }
                            }
                            else
                            {
                                // This is used offline in Testing
                                PUNTakeItem(hasTakenItem);
                            }
                        }
                    }
                }
                else
                {
                    //Debug.Log("Drops the item");
                    if (grabberHeld == false)
                    {
                        hasTakenItem = false;
                        if (PhotonNetwork.connectionState == ConnectionState.Connected && photonView != null)
                        {
                            // If I'm the one taking the item an
                            if (photonView.isMine)
                            {
                                // When the player stops grabbing the item or decides to drop it, executes the RPC to take the item and notify the other players of that event.
                                photonView.RPC("PUNTakeItem", PhotonTargets.All, hasTakenItem);
                            }
                        }
                        else
                        {
                            // This is used offline in Testing
                            PUNTakeItem(hasTakenItem);
                        }
                    }
                }
            }
        }       
    }

    /// <summary>
    /// When the player has taken / dropped an Item, takes / drop the item and then notifies the other players of this action
    /// </summary>
    /// <param name="toTakeItem">True if the player must take it. False if the player must drop it</param>
    [PunRPC]
    public void PUNTakeItem(bool toTakeItem)
    {
        // If the player must take the item, then takes it changes it position and rotation in the avatar's left hand.
        if (toTakeItem == true)
        {
            SetInteractiveItemColliders(false, possibleItem);
            SetInteractiveItemAnimation(false, possibleItem);
            possibleItem.transform.parent = grabPosition;
            possibleItem.transform.localPosition = Vector3.zero;
            possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }
        else
        {
            // The player must drop the item
            if (possiblePedestalToDropPlace != null) 
            {
                // If there is an altar close, we check if the altar and the item are of the same color. If they are, we solve that puzzle. If not, we drop the item
                PuzzleSolver puzzleSolver = possiblePedestalToDropPlace.GetComponentInParent<PuzzleSolver>();
                bool answer = puzzleSolver.checkPuzzle(possibleItem);
                //Debug.Log("Is Same Color: " + answer);
                if (answer == true)
                {
                    // The item and the color are of the same color
                    PhotonView dropOffPhotonView = possiblePedestalToDropPlace.GetComponent<PhotonView>();
                    if (PhotonNetwork.connectionState == ConnectionState.Connected && dropOffPhotonView != null)
                    {
                        //Debug.Log("We notify the altar that the puzzle has been solved");

                        // We notify the altar script that the puzzle has been solved and then we change the position of the item from the player's hand to the altar.
                        dropOffPhotonView.RPC("solvePuzzle", PhotonTargets.All);
                        photonView.RPC("PossibleItemSolved", PhotonTargets.All);
                    }
                    else
                    {
                        // This is used offline in Testing
                        puzzleSolver.solvePuzzle();
                        PossibleItemSolved();
                        Debug.Log("Is Offline");
                    }
                }
                else
                {
                    // If they are not of the same color, we enable the item colliders, we set the item as a child of the world and reset its status so it can be grabbed again later.
                    DropItem();
                    Debug.Log("Item Dropped");
                }

            }
            else
            {
                // If there is no altar, we just drop the item into the world.
                DropItem();
            }
            possibleItem = null;
        }
    }

    /// <summary>
    /// Method that drops the item into the world.
    /// We set the item as a child of the world instead of the left hand of the player. We reset its colliders, transform and animation.
    /// </summary>
    public void DropItem()
    {
        SetInteractiveItemColliders(true, possibleItem);
        SetInteractiveItemAnimation(true, possibleItem);
        possibleItem.transform.parent = GameObject.Find("InteractiveItems").transform;
        possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);

        Vector3 originalHeight = possibleItem.transform.position;
        originalHeight.y = transform.position.y;
        possibleItem.transform.position = originalHeight;
    }

    /// <summary>
    /// Used to activate the colliders of the key. This allows that when the player comes close to a pedestal, the key can be recognized
    /// </summary>
    /// <param name="valueP"></param>
    /// <param name="interactiveItem"></param>
    public void SetInteractiveItemColliders(bool valueP, GameObject interactiveItem)
    {
        InteractiveItem item = interactiveItem.GetComponent<InteractiveItem>();
        item.setItem(valueP);
    }

    /// <summary>
    /// Activates/Deactivates the key animation. Usually the key is hovering, when the player takes it, this method stops the animation and when he leaves it, it re-starts the animation
    /// </summary>
    /// <param name="valueP"></param>
    /// <param name="interactiveItem"></param>
    public void SetInteractiveItemAnimation(bool valueP, GameObject interactiveItem)
    {
        InteractiveItem item = interactiveItem.GetComponent<InteractiveItem>();
        item.setAnimation(valueP);
    }

    /// <summary>
    /// When the player has left the key in the right pedestal. Deactivates its colliders and lets the item hovering forever
    /// </summary>
    [PunRPC]
    public void PossibleItemSolved()
    {
        possibleItem.transform.parent = possiblePedestalToDropPlace.transform;
        Vector3 newVectorZero = Vector3.zero;
        newVectorZero.y = 1.6f;
        possibleItem.transform.localPosition = newVectorZero;
        possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);
        SetInteractiveItemAnimation(true, possibleItem);
        possibleItem = null;
        possiblePedestalToDropPlace = null;
    }

    // This method is not required for this project but it is required by Photon
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // throw new NotImplementedException();
    }
}
