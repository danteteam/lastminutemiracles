﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the navigation of the player.
/// The player can walk or teleport to a position in a determined range.
/// </summary>
public class PlayerMotion : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the animations of an avatar
    public AnimationManager animationManager;

    // Current Device of the player
    public CurrentPlatform currentPlatform;

    // Bool the defines if the player can move or not
    public bool busyToWalk;

    // Class that manages the connection to the database.
    public DBHandler DBHandler;

    // GameObject of the Teleporter Marker
    public GameObject teleportMarker;

    // Point from which the raycast will originate
    public Transform rayCastOrigin;

    // Length of the Ray Cast. This determines the range of the places where the player can move to
    public float RayLength;

    // Rate at which the player will be walking when teleporting
    public float walkingSpeed;

    // Current mode for the avatar to teleport or to move. 0 == AVATAR MOVES. 1 == AVATAR TELEPORTS
    public int currentMode;

    // Particle System for teleporting. [0] For Player. [1] For Marker
    public GameObject[] teleportSpirals;

    //---------------------------------------
    // Private Variables
    //---------------------------------------

    // Main Camera of the Player
    private GameObject mainCameraObject;

    // Bool that defines if the marker for teleporting is active or not
    private bool markerActive;

    // Bool that defines if the player can move or not
    private bool canMove;

    // Has the player changed his navigation mode
    private bool HasChanged;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start()
    {
        SwitchNavMode();
        DBHandler.device = currentPlatform.platform.ToString();
        HasChanged = false;
    }

    /// <summary>
    /// Switches between walking and teleporting navigation mode
    /// </summary>
    public void SwitchNavMode()
    {
        currentMode = PlayerPrefs.GetInt("NavMode");
        if (currentMode == 1) 
        {
            currentMode = 0; // Mode A - Walking
        }
        else if (currentMode == 0)
        {
            currentMode = 1;  // Mode B - Teleporting
        }
        //currentMode = 1;
        PlayerPrefs.SetInt("NavMode", currentMode);
        HasChanged = true;
    }

    /// <summary>
    /// Method that updates the camera position
    /// </summary>
    /// <param name="camera">Camera of the player</param>
    public void MoveCamera(Transform camera)
    {
        mainCameraObject = camera.gameObject;
        Ray ray = new Ray();
        if (currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.OCULUS_TOUCH || currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.HTC_VIVE)
        {
            ray = new Ray(rayCastOrigin.position, rayCastOrigin.forward);
            //Debug.DrawRay(rayCastOrigin.position, rayCastOrigin.forward, Color.yellow);

        }
        else if (currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.GEAR_VR || currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.PC)
        {
            ray = new Ray(camera.position, camera.forward);
            //Debug.DrawRay(camera.position, camera.forward, Color.yellow);
        }
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, RayLength))
        {
            //Debug.Log("Ray Hitted with: " + hit.collider.tag);
            Vector3 rayPoint = hit.point;
            rayPoint.y += 0.1f;
            if (hit.collider.tag == "Floor")
            {
               // Debug.Log("Can move to here");
                //busyToWalk = false;
                canMove = true;
                if (!teleportMarker.activeSelf)
                {
                    teleportMarker.SetActive(true);
                }
                teleportMarker.transform.position = rayPoint;
            }
            else
            {
               // Debug.Log("Shouldn't move here");
                //busyToWalk = true;
                teleportMarker.SetActive(false);
                canMove = false;
            }
        }
    }

    /// <summary>
    /// Method that enables/disables the marker
    /// </summary>
    /// <param name="newValue">New Status for the Marker</param>
    public void SetMarkerStatus(bool newValue)
    {
        bool theValue = newValue;
        if (busyToWalk == true)
        {
            theValue = false;
        }
        markerActive = theValue;
        teleportMarker.transform.GetChild(0).gameObject.SetActive(theValue);

        if(currentMode == 0 && theValue == true)
        {
            teleportSpirals[1].SetActive(false);
        }
        else if(currentMode == 1 && theValue == true)
        {
            teleportSpirals[1].SetActive(true);
        }
        else
        {
            teleportSpirals[1].SetActive(false);
        }
    }

    /// <summary>
    /// Method that executes the motion action for this type of interaction
    /// </summary>
    public void MotionAction()
    {
        PUNTeleportPlayer();
    }

    /// <summary>
    /// Method that makes the player move where he is aiming at. Then activates the walking or teleporting animation. At last notifies the other players of this action.
    /// </summary>
    [PunRPC]
    public void PUNTeleportPlayer()
    {
        if (markerActive == true && busyToWalk == false && canMove == true)
        {
            Vector3 newPosition = teleportMarker.transform.position;

            // We reset the y position so the player doesn't go underground
            if(currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.GEAR_VR || currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.OCULUS_TOUCH || currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.PC)
            {
                newPosition.y = mainCameraObject.transform.parent.position.y;
            }
            else if(currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.HTC_VIVE)
            {
                newPosition.y = mainCameraObject.transform.parent.parent.position.y;
            }

            GameObject parent = GetLastParent(mainCameraObject);

            switch (currentMode)
            {
                case 0: // AVATAR WALKS
                    StartCoroutine(WalkingCoroutine(parent, newPosition));
                    break;
                case 1: // AVATAR TELEPORTS
                    StartCoroutine(TeleportingRoutine(parent, newPosition));
                    break;
            }
        }
    }

    /// <summary>
    /// Method that gets the last camera parent
    /// </summary>
    /// <param name="Child"></param>
    /// <returns></returns>
    public GameObject GetLastParent(GameObject Child)
    {
        GameObject lastParent = null;
        bool hasFound = false;
        while(!hasFound)
        {
            lastParent = Child;
            Transform temp = Child.transform.parent;
            if(temp == null)
            {
                hasFound = true;
            }
            else
            {
                Child = Child.transform.parent.gameObject;
            }
        }

        return lastParent;        
    }

    /// <summary>
    /// Coroutine used to simulate the walking animation of the player
    /// </summary>
    /// <param name="origin">Origin from where the player will start walking</param>
    /// <param name="destination">Destination to where the player will walk towards to</param>
    /// <returns></returns>
    IEnumerator WalkingCoroutine(GameObject origin, Vector3 destination)
    {
        bool hasArrived = false;
        animationManager.setBoolAnim(true, 2, "Walk", true);
        busyToWalk = true;
        SetMarkerStatus(false);
        while (hasArrived == false)
        {
            origin.transform.position = Vector3.MoveTowards(origin.transform.position, destination, Time.deltaTime * walkingSpeed);
            if (origin.transform.position == destination)
            {
                hasArrived = true;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
        busyToWalk = false;
        SetMarkerStatus(true);
        animationManager.setBoolAnim(true, 2, "Walk", false);
        DBHandler.AModeJumps++;
    }

    /// <summary>
    /// Coroutine used to simulate the teleporting animation of the player
    /// </summary>
    /// <param name="origin">Origin from where the player will teleport</param>
    /// <param name="destination">Destination to where the player will teleport to</param>
    /// <returns></returns>
    IEnumerator TeleportingRoutine(GameObject origin, Vector3 destination)
    {
        //Debug.Log("Is Teleporting Loco D: ");
        teleportSpirals[0].SetActive(true);
        busyToWalk = true;
        SetMarkerStatus(false);
        float distance = Vector3.Distance(destination, origin.transform.position);
        float possibleTime = distance / walkingSpeed;
        //Debug.Log("Posible tiempo es: " + possibleTime);

        yield return new WaitForSeconds(possibleTime);
        origin.transform.position = destination;

        teleportSpirals[0].SetActive(false);
        busyToWalk = false;
        SetMarkerStatus(true);
        //DBHandler.BModeJumps++;
    }

    /// <summary>
    /// Switches the navigation mode or finishes the game when the player enters a specific area
    /// </summary>
    /// <param name="other">The GameObject of the area where the player has arrived</param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "SwitchMode" && !HasChanged)
        {
            SwitchNavMode();
            //DBHandler.firstLvlCompleted = true;
        }
        else if(other.tag == "WinUmbral")
        {
            //DBHandler.secondLvlCompleted = true;
            float totalTimeX = other.gameObject.GetComponent<WinnersAnnouncement>().totalTime;
            //DBHandler.sessionLength = totalTimeX + "";
            //DBHandler.send();
        }
    }

    // This method is not required for this project but it is required by Photon
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
