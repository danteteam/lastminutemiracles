﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ----------------------------
/// Project Summary:
/// This is for a Multiplayer VR Online Game. Two players have to pick up eggs (here known as items) of a specific color and place them
/// in altars that open doors. The goal is to open all the doors and get to the end of the game.
///
/// All these scripts handle the inputs, recognizes them and execute an action of movement or interaction.
/// What I really like is that no matter what device you are using you will always use the same classes for interaction
/// and movement. 
/// ----------------------------
/// Interaction Manager for the Gear Users. This class allows the player to move with his right hand 
/// by pointing out a place in the floor to where he wants to move and take/drop an item with his left hand
/// </summary>
public class PCInteractionManager : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the oculus inputs
    public PCInputManager pcInputManager;

    //---------------------------------------
    // Switchable Interactions Variables
    //---------------------------------------

    // Enum for the different interaction modes available to the device
    public enum CURRENT_PC_MODE { INTERACTION, MOVEMENT };

    // Current interaction Mode
    public CURRENT_PC_MODE currentMode;

    // Script that handles the teleportation process for the right arm
    public PlayerMotion playerMotionManager;

    // Script that handles the interaction process for the left arm
    public PlayerInteraction playerInteractionManager;

    //---------------------------------------
    // Private Variables
    //---------------------------------------
    /// <summary>
    /// Tells if the player has taken the key
    /// </summary>
    private bool hasTakenItem;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start ()
    {
        string[] nameSplit = transform.parent.gameObject.name.Split(' ');
        string number = nameSplit[1];
        GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject temp in array)
        {
            if (temp.name.Contains("Camera") && temp.name.Contains(number))
            {
                pcInputManager = temp.GetComponent<PCInputManager>();
                break;
            }
        } 

        if (pcInputManager != null)
        {
            pcInputManager.OnCameraMoveEvent += OnCameraMoved;
            pcInputManager.OnClickDownEvent += OnClickButtonDown;
            pcInputManager.OnRightClickEvent += OnSwipeRight;
            pcInputManager.OnLeftClickEvent += OnSwipeLeft;
        }
        OnSwipeRight();
	}

    /// <summary>
    /// Method used when the Gear user swipes left on the device
    /// </summary>
    public void OnSwipeLeft()
    {
        if (currentMode != CURRENT_PC_MODE.INTERACTION)
        {
            currentMode = CURRENT_PC_MODE.INTERACTION;
        }
        playerMotionManager.animationManager.setBoolAnim(true,1, "ClosedHand", false);
        // Camera Anims
        playerMotionManager.animationManager.setBoolAnim(false, 0, "LeftUp", true);
        playerMotionManager.animationManager.setBoolAnim(false, 1, "RUP", false);

        playerMotionManager.SetMarkerStatus(false);
    }

    /// <summary>
    /// Method used when the Gear user swipes right on the device
    /// </summary>
    public void OnSwipeRight()
    {
        if(currentMode != CURRENT_PC_MODE.MOVEMENT)
        {
            currentMode = CURRENT_PC_MODE.MOVEMENT;
        }
        playerMotionManager.animationManager.setBoolAnim(true,1, "ClosedHand", true);
        // Camera Anims
        playerMotionManager.animationManager.setBoolAnim(false, 0, "LeftUp", false);
        playerMotionManager.animationManager.setBoolAnim(false, 1, "RUP", true);
        playerMotionManager.SetMarkerStatus(true);
    }

    /// <summary>
    /// Method that happends when the central button of the Gear VR has been touched
    /// </summary>
    public void OnClickButtonDown()
    {
        switch(currentMode)
        {
            case CURRENT_PC_MODE.MOVEMENT:
                playerMotionManager.MotionAction();                
                break;
            case CURRENT_PC_MODE.INTERACTION:
                if(hasTakenItem == false)
                {
                    hasTakenItem = true;
                }
                else
                {
                    hasTakenItem = false;
                }
                playerInteractionManager.TakeItem(hasTakenItem);
                playerMotionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", hasTakenItem);
                break;
        }
    }

    /// <summary>
    /// Method used to update the camera position 
    /// </summary>
    /// <param name="playerCamera">The Camera Position</param>
    public void OnCameraMoved(Transform playerCamera)
    {
        playerMotionManager.MoveCamera(playerCamera);
    }

    // This method is not required for this project but it is required by Photon
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
