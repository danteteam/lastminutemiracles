﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used to define the anchors of the player model
/// </summary>
public class AnchorsManagement : MonoBehaviour
{
    public Transform[] anchors;	
}
