﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that Synchronizes the camera with the player body
/// </summary>
public class SyncCameraBody : MonoBehaviour
{
    //------------------------------
    // Methods
    //------------------------------
    // Use this for initialization
    void Start()
    {
        if(GameObject.Find("IsTesting") != null)
        {
            GameObject.Find("HTCCamera").GetComponent<OculusInputManager>().OnCameraMoveEvent += syncPlayerCamera;
        }
        else
        {
            string[] nameSplit = name.Split(' ');
            string number = nameSplit[1];
            GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject temp in array)
            {
                if (temp.name.Contains("Camera") && temp.name.Contains(number))
                {
                    switch(tag)
                    {
                        case "GearPlayer":
                            temp.GetComponent<OculusInputManager>().OnCameraMoveEvent += syncPlayerCamera;
                            break;
                        case "HTCPlayer":
                            temp.GetComponent<HTCInputManager>().OnCameraMoveEvent += syncPlayerCamera;
                            break;
                        case "PCPlayer":
                            temp.GetComponent<PCInputManager>().OnCameraMoveEvent += syncPlayerCamera;
                            break;
                    }
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Method that synchronizes the position and rotation of the camera with the body. 
    /// </summary>
    /// <param name="cameraTransform"></param>
    public void syncPlayerCamera(Transform cameraTransform)
    {
        // Here we syncronize the camera with the player transform
        Vector3 newPlayerPos = cameraTransform.position;
        //newPlayerPos.z += 0.1f;
        newPlayerPos.y -= 1.68f;
        transform.position = newPlayerPos;

        // Here we synchronize the camera rotation
        Vector3 newPlayerRot = transform.rotation.eulerAngles;
        newPlayerRot.y = cameraTransform.rotation.eulerAngles.y;
        transform.eulerAngles = newPlayerRot;
    }
}
