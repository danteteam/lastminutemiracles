﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Inverse Kinematics Control. Class in charge of the inverse kinematics for the hands
/// </summary>
[RequireComponent(typeof(Animator))]
public class IKControl : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------
    
    // Main Animator of the player
    protected Animator animator;

    // Bool that defines if the ik is going to be active
    public bool ikActive = false;

    // Transform for the right ik hand
    public Transform rightHandObj = null;

    // Transform for the left ik hand
    public Transform leftHandObj = null;

    // Transform for the ik head
    public Transform lookHeadObj = null;

    //---------------------------------------
    // Methods
    //---------------------------------------
    void Start()
    {
        animator = GetComponent<Animator>();
        if (GameObject.Find("IsTesting") == null)
        {
            setParts();
        }
        if(GetComponent<CurrentPlatform>().platform == CurrentPlatform.CURRENT_PLATFORM.PC)
        {
            this.transform.position= new Vector3(this.transform.position.x, 54.4f, this.transform.position.z);
        }
    }

    /// <summary>
    /// Method that sets the transform of the current parts 
    /// </summary>
    public void setParts()
    {
        GameObject[] playerParts = GameObject.FindGameObjectsWithTag(tag);
        foreach(GameObject temp in playerParts)
        {
            string[] nameSplit = name.Split(' ');
            string number = nameSplit[1];
            if (temp.name.Contains("Camera") && temp.name.Contains(number))
            {
                AnchorsManagement anch = temp.GetComponent<AnchorsManagement>();
                lookHeadObj = anch.anchors[0];
                leftHandObj = anch.anchors[1];
                rightHandObj = anch.anchors[2];
            }
        }
    }

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {

            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {

                // Set the look target position, if one has been assigned
                if (lookHeadObj != null)
                {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookHeadObj.position);
                }

                // Set the right hand target position and rotation, if one has been assigned
                if (rightHandObj != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                }
                if (leftHandObj != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandObj.rotation);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
