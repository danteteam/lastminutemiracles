﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used to manage the animations of the players
/// </summary>
public class AnimationManager : Photon.PunBehaviour
{
    //--------------------------------------
    // Public Variables
    //--------------------------------------

    // Array of Animators 
    public Animator[] playerAnimators;

    // Array of Animators inside the Camera
    public Animator[] cameraAnimators;

    //--------------------------------------
    // Methods
    //--------------------------------------

    void Start()
    {
        if(cameraAnimators.Length > 0)
        {
            GameObject camAnim1 = GameObject.Find("LHandIKPoint");
            GameObject camAnim2 = GameObject.Find("RHandIKPoint");
            cameraAnimators[0] = camAnim1.GetComponent<Animator>();
            cameraAnimators[1] = camAnim2.GetComponent<Animator>();
        }
    }

    /// <summary>
    /// Method that changes the state of a bool animation
    /// </summary>
    /// <param name="animIndex">Index inside the array of animations</param>
    /// <param name="animParameter">Name of the parameter</param>
    /// <param name="newValue">value of the parameter</param>
    public void setBoolAnim(bool isPlayer, int animIndex, string animParameter, bool newValue)
    {
        switch (isPlayer)
        {
            case true:
                playerAnimators[animIndex].SetBool(animParameter, newValue);
                break;
            case false:
                cameraAnimators[animIndex].SetBool(animParameter, newValue);
                break;
        }
    }

    /// <summary>
    /// Method that changes triggers a new animation
    /// </summary>
    /// <param name="animIndex">Index inside the array of animations</param>
    /// <param name="animParameter">Name of the parameter</param>
    public void setTriggerAnim(bool isPlayer, int animIndex, string animParameter)
    {
        switch (isPlayer)
        {
            case true:
                playerAnimators[animIndex].SetTrigger(animParameter);
                break;
            case false:
                cameraAnimators[animIndex].SetTrigger(animParameter);
                break;
        }
    }
}
