﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that defines the color for the player model. The color changes when the player steps on a new color Platform
/// </summary>
public class PlayerColor : Photon.PunBehaviour, IPunObservable
{
    //-----------------------------------------------
    // Public Variables
    //-----------------------------------------------
    /// <summary>
    /// Mesh Renderer of the player to which we will apply a new color
    /// </summary>
    public SkinnedMeshRenderer playerMeshRenderer;

    /// <summary>
    /// Current color of the player
    /// </summary>
    public ObjectColor.ITEM_COLOR currentColor;

    /// <summary>
    /// Color of the material in the mesh renderer
    /// </summary>
    private Color32 playerMatcolor;

    //-----------------------------------------------
    // Methods
    //-----------------------------------------------
    // Use this for initialization
    void Start ()
    {
        playerMatcolor = new Color32(255, 255, 255, 255);
        currentColor = ObjectColor.ITEM_COLOR.WHITE;
        playerMeshRenderer.material.color = playerMatcolor;
	}

    /// <summary>
    /// Sets a new color to the mesh renderer of the player
    /// </summary>
    /// <param name="newColor"></param>
    [PunRPC]
    public void setNewcolor(Color32 newColor)
    {
        playerMatcolor = newColor;
        Material neoMaterial = playerMeshRenderer.material;
        neoMaterial.color = playerMatcolor;
        playerMeshRenderer.material.color = newColor;
    }

    /// <summary>
    /// Method called when the player enters a color platform to switch his color
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if(other.name == "Platform")
        {
            ObjectColor neoColor = other.gameObject.GetComponent<ObjectColor>();
            setNewcolor(neoColor.getColor());
            currentColor = neoColor.currentColor;
            other.gameObject.SetActive(false);
           // photonView.RPC("setNewColor", PhotonTargets.All, neoColor.getColor());
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
