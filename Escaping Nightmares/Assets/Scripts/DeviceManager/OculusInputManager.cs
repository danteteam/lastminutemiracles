﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the inputs for the Oculus Rift and Gear VR
/// I really like this class because using delegates makes it easier to handle the user's inputs and execute certain action
/// </summary>
public class OculusInputManager : Photon.PunBehaviour
{
    //---------------------------------------
    // Delegates and Events
    //---------------------------------------  
    /// <summary>
    /// Main Camera used by the Oculus or Gear
    /// </summary>
    public GameObject mainCamera;

    //---------------------------------------
    // Delegates and Events
    //---------------------------------------   
    /// <summary>
    /// Delegate for when the Oculus Camera is moving in the real space. It is used to synchronize the avatar position in-game
    /// </summary>
    /// <param name="cameraTransform"></param>
    public delegate void OnCameraMove(Transform cameraTransform);
    public event OnCameraMove OnCameraMoveEvent;

    // Gear VR---------------------------------------
    /// <summary>
    /// Delegate used to know when the player has touched the input part of the Gear
    /// </summary>
    public delegate void OnTouchDown();
    public event OnTouchDown OnTouchDownEvent;

    /// <summary>
    /// Delegate used to know when the player has swipped left. When the player swipes left, it activates the picking/dropping items mode.
    /// </summary>
    public delegate void OnTouchLeft();
    public event OnTouchLeft OnTouchLeftEvent;

    /// <summary>
    /// Delegate used to know when the player has swipped right. When the player swipes right, it activates the navigation mode.
    /// </summary>
    public delegate void OnTouchRight();
    public event OnTouchRight OnTouchRightEvent;

    // Oculus Touch------------------------------------

    /// <summary>
    /// Delegate for when the Grabber of the right hand is used. This is used to close the fist in the avatar and enable aiming to teleport
    /// </summary>
    /// <param name="isGrabbing"></param>
    public delegate void OnRightGrabber(bool isGrabbing);
    public event OnRightGrabber OnRightGabbrerBegin;

    /// <summary>
    /// Delegate for when the Grabber of the left hand has been used. This is used when the player wants to take a key.
    /// </summary>
    /// <param name="isGrabbing"></param>
    public delegate void OnLeftGrabber(bool isGrabbing);
    public event OnLeftGrabber OnLeftGrabberBegin;

    /// <summary>
    /// Delegate for when the right trigger has been pressed. This is used when the player is aiming with its right hand and wants to move somewhere.
    /// </summary>
    public delegate void OnRightTrigger();
    public event OnRightTrigger OnRightTriggerBegin;

    //---------------------------------------
    // Private Variables
    //---------------------------------------   

    /// <summary>
    /// When the player has pressed the right trigger, applies a lock that doesn't allow multiple jumps/movements actions.
    /// </summary>
    private bool rightTriggerLock;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Update is called once per frame
    void FixedUpdate()
    {
        // We validate that we are connected to the game session and that I'm the owner of the session.
        // This allows that only I as an Gear VR User can use the inputs of this cellphone / PC (if it is an Oculus Rift)
        if ((PhotonNetwork.connected == true && photonView != null && photonView.isMine))
        {
            if(OnCameraMoveEvent != null)
            {
                OnCameraMoveEvent(mainCamera.transform);
            }
            switch (GetComponent<CurrentPlatform>().platform)
            {
                case CurrentPlatform.CURRENT_PLATFORM.OCULUS_TOUCH: // OCULUS TOUCH
                    if(OnRightGabbrerBegin != null)
                    {
                        float rightGrabberValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch);
                        //Debug.Log("Grab Right Value: " + rightGrabberValue);
                        if (rightGrabberValue > 0.85f)
                        {
                            OnRightGabbrerBegin(true);
                        }
                        else
                        {
                            OnRightGabbrerBegin(false);
                        }
                    }

                    if(OnLeftGrabberBegin != null)
                    {
                        float leftGrabberValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch);
                        if (leftGrabberValue > 0.85f)
                        {
                            OnLeftGrabberBegin(true);
                        }
                        else if (leftGrabberValue > 0f && leftGrabberValue < 0.1)
                        {
                            OnLeftGrabberBegin(false);
                        }
                    }


                    if (rightTriggerLock == false && OnRightTriggerBegin != null)
                    {
                        float rightShootTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
                        if (rightShootTrigger > 0.9f)
                        {
                            StartCoroutine(ApplyRightTriggerDelay(0.8f));
                            OnRightTriggerBegin();
                        }
                    }

                    break;
                case CurrentPlatform.CURRENT_PLATFORM.GEAR_VR: // GEAR VR
                    // If im the local player or if i'm in a menu receive oculus input
                    if (OVRInput.GetDown(OVRInput.Button.One) && OnTouchDownEvent != null || Input.GetMouseButton(0))
                    {
                        if(rightTriggerLock == false && OnTouchDownEvent != null)
                        {
                            OnTouchDownEvent();
                            StartCoroutine(ApplyRightTriggerDelay(0.8f));
                        }
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Left) && OnTouchLeftEvent != null)
                    {
                        OnTouchLeftEvent();
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Right) && OnTouchRightEvent != null)
                    {
                        OnTouchRightEvent();
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Enumerator that applies a delay when the right trigger has been pressed
    /// </summary>
    /// <param name="delay">Time of Delay</param>
    /// <returns>Nothing</returns>
    IEnumerator ApplyRightTriggerDelay(float delay)
    {
        rightTriggerLock = true;
        yield return new WaitForSeconds(delay);
        rightTriggerLock = false;
    }
}
