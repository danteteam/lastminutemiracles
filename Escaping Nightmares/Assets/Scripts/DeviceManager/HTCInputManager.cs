﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the inputs of the HTC Vive Device.
/// I really like this class because using delegates makes it easier to handle the user's inputs and execute certain action
/// </summary>
public class HTCInputManager : Photon.PunBehaviour
{
    //-----------------------------------------------------------
    // Public Variables
    //-----------------------------------------------------------
    /// <summary>
    /// HTC Camera
    /// </summary>
    public GameObject HTCCamera;

    /// <summary>
    /// GameObject of the Right Hand
    /// </summary>
    public GameObject rightHand;

    /// <summary>
    /// Tracked Control for the right hand
    /// </summary>
    private SteamVR_TrackedObject RTracked;

    /// <summary>
    /// Right HTC Controller
    /// </summary>
    private SteamVR_Controller.Device RController
    {
        get { return SteamVR_Controller.Input((int)RTracked.index); }
    }

    /// <summary>
    /// Game Object of the Left Hand
    /// </summary>
    public GameObject leftHand;

    /// <summary>
    /// Tracked Control for the left hand
    /// </summary>
    private SteamVR_TrackedObject LTracked;

    /// <summary>
    /// Left HTC Controller
    /// </summary>
    private SteamVR_Controller.Device LController
    {
        get { return SteamVR_Controller.Input((int)LTracked.index); }
    }

    //-----------------------------------------------------
    // Delegates
    //-----------------------------------------------------
    /// <summary>
    /// Delegate for when the HTC Camera is moving in the real space. It is used to synchronize the avatar position in-game
    /// </summary>
    /// <param name="cameraPosition"></param>
    public delegate void OnCameraMove(Transform cameraPosition);
    public event OnCameraMove OnCameraMoveEvent;

    /// <summary>
    /// Delegate for when the Grabber of the right hand is used. This is used to close the fist in the avatar and enable aiming to teleport
    /// </summary>
    /// <param name="isGrabbing"></param>
    public delegate void OnRightGrabber(bool isGrabbing);
    public event OnRightGrabber OnRightGrabberEvent;

    /// <summary>
    /// Delegate for when the right trigger has been pressed. This is used when the player is aiming with its right hand and wants to move somewhere.
    /// </summary>
    public delegate void OnRightTrigger();
    public event OnRightTrigger OnRightTriggerEvent;

    /// <summary>
    /// Delegate for when the left trigger has been pressed. This is used when the player wants to take a key.
    /// </summary>
    /// <param name="isGrabbing"></param>
    public delegate void OnLeftTrigger(bool isGrabbing);
    public event OnLeftTrigger OnLeftTriggerEvent;

    //-----------------------------------------------------
    // Methods
    //-----------------------------------------------------

    // Use this for initialization
    void Awake ()
    {
        RTracked = rightHand.GetComponent<SteamVR_TrackedObject>();
        LTracked = leftHand.GetComponent<SteamVR_TrackedObject>();
	}

    // Use this for initialization
    private void Start()
    {
        Vector3 currentPost = transform.position;
        currentPost.y -= 1.8f;
        transform.position = currentPost;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        // We validate that we are connected to the game session and that I'm the owner of the session.
        // This allows that only I as an HTC User can use the inputs of the device connected to this PC.
        if ((PhotonNetwork.connected == true && photonView != null && photonView.isMine))
        {
            if (OnCameraMoveEvent != null)
            {
                //Debug.Log("Camera is: " + HTCCamera.transform.position.ToString());
                OnCameraMoveEvent(HTCCamera.transform);
            }
            if (LController.GetHairTriggerDown() && OnLeftTriggerEvent != null)
            {
                //Debug.Log(gameObject.name + " Trigger Left Something");
                OnLeftTriggerEvent(true);
            }
            else if (LController.GetHairTriggerUp() && OnLeftTriggerEvent != null)
            {
                //Debug.Log(gameObject.name + " Trigger Left Something");
                OnLeftTriggerEvent(false);
            }

            if (RController.GetHairTriggerDown() && OnRightTriggerEvent != null)
            {
                // Debug.Log(gameObject.name + " Trigger Right Something");
                OnRightTriggerEvent();
            }

            if (RController.GetPressDown(SteamVR_Controller.ButtonMask.Grip) && OnRightGrabberEvent != null)
            {
                OnRightGrabberEvent(true);
            }
            else if(RController.GetPressUp(SteamVR_Controller.ButtonMask.Grip) && OnRightGrabberEvent != null)
            {
                OnRightGrabberEvent(false);
            }
        }            
    }
}
