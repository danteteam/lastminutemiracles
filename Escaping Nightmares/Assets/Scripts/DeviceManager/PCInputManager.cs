﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the inputs for the PC
/// I really like this class because using delegates makes it easier to handle the user's inputs and execute certain action
/// </summary>
public class PCInputManager : Photon.PunBehaviour
{
    //---------------------------------------
    // Delegates and Events
    //---------------------------------------  
    /// <summary>
    /// Main Camera used by the Oculus or Gear
    /// </summary>
    public GameObject mainCamera;

    //---------------------------------------
    // Delegates and Events
    //---------------------------------------   
    /// <summary>
    /// Delegate for when the PC Camera is moving in the real space. It is used to synchronize the avatar position in-game
    /// </summary>
    /// <param name="cameraTransform"></param>
    public delegate void OnCameraMove(Transform cameraTransform);
    public event OnCameraMove OnCameraMoveEvent;

    /// <summary>
    /// Delegate used to know when the player has clicked the first mouse button
    /// </summary>
    public delegate void OnClickDown();
    public event OnClickDown OnClickDownEvent;

    /// <summary>
    /// Delegate used to know when the player has pressed the left key. When the player presses the left key, it activates the picking/dropping items mode.
    /// </summary>
    public delegate void OnLeftClick();
    public event OnLeftClick OnLeftClickEvent;

    /// <summary>
    /// Delegate used to know when the player has pressed the right key. When the player presses the right key, it activates the navigation mode.
    /// </summary>
    public delegate void OnRightClick();
    public event OnRightClick OnRightClickEvent;

    //---------------------------------------
    // Private Variables
    //---------------------------------------   

    /// <summary>
    /// When the player has pressed the right trigger, applies a lock that doesn't allow multiple jumps/movements actions.
    /// </summary>
    private bool rightTriggerLock;

    private float yaw;

    private float pitch;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Update is called once per frame
    void FixedUpdate()
    {
        // We validate that we are connected to the game session and that I'm the owner of the session.
        // This allows that only I as an Gear VR User can use the inputs of this cellphone / PC (if it is an Oculus Rift)
        if ((PhotonNetwork.connected == true && photonView != null && photonView.isMine))
        {
            if(GetComponent<CurrentPlatform>().platform == CurrentPlatform.CURRENT_PLATFORM.PC)
            {
                if (OnCameraMoveEvent != null)
                {
                    yaw += 5f * Input.GetAxis("Mouse X");
                    pitch -= 5f * Input.GetAxis("Mouse Y");

                    mainCamera.transform.eulerAngles = new Vector3(pitch, yaw, 0f);

                    OnCameraMoveEvent(mainCamera.transform);
                }
                // If im the local player or if i'm in a menu receive oculus input
                if (Input.GetMouseButtonDown(0) && OnClickDownEvent != null)
                {
                    if (rightTriggerLock == false)
                    {
                        OnClickDownEvent();
                        StartCoroutine(ApplyRightTriggerDelay(0.8f));
                    }
                }
                else if ( ( Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) ) && OnLeftClickEvent != null)
                {
                    OnLeftClickEvent();
                }
                else if ( (Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) ) && OnRightClickEvent != null)
                {
                    OnRightClickEvent();
                }
            }
        }
    }

    /// <summary>
    /// Enumerator that applies a delay when the right trigger has been pressed
    /// </summary>
    /// <param name="delay">Time of Delay</param>
    /// <returns>Nothing</returns>
    IEnumerator ApplyRightTriggerDelay(float delay)
    {
        rightTriggerLock = true;
        yield return new WaitForSeconds(delay);
        rightTriggerLock = false;
    }
}
