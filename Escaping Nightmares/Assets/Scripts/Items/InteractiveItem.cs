﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class for the interactive items in the game
/// </summary>
public class InteractiveItem : Photon.PunBehaviour
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------
    /// <summary>
    /// Color of the item
    /// </summary>
    public ObjectColor.ITEM_COLOR currentColor;

    // Sphere Collider of the Item
    public SphereCollider itemSphereCollider;
    
    //---------------------------------------
    // Methods
    //---------------------------------------

    /// <summary>
    /// Method that enables/disables the colliders of the item
    /// </summary>
    /// <param name="valueP">Value to be set to the colliders</param>
    public void setItem(bool valueP)
    {
        itemSphereCollider.enabled = valueP;
    }
    
    /// <summary>
    /// Method that modifies the item animation
    /// </summary>
    /// <param name="valueP"></param>
    public void setAnimation(bool valueP)
    {
        GetComponentInChildren<Animator>().SetBool("Moving", valueP);
    }
}
