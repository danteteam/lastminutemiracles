﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class used to store the statistics of the session in a Google Form
/// </summary>
public class DBHandler : Photon.PunBehaviour, IPunObservable
{
    //--------------------------------------
    // Private Variables
    //--------------------------------------
    // URL of the Google Form
    private string URL = "https://docs.google.com/forms/d/e/1FAIpQLSeGqpRa5PeDZ3tvFnQvsg1TvLYymHeF_6Ra8NB2OahxJ9aIsQ/formResponse";
    
    // Has the system send the form or not
    private bool finalSend;

    //--------------------------------------
    // Public Variables
    //--------------------------------------
    /// <summary>
    /// Information of the session
    /// </summary>
    public string sessionInfo;

    /// <summary>
    /// Device used by the current player
    /// </summary>
    public string device;

    /// <summary>
    /// Length of the session
    /// </summary>
    public string sessionLength;

    /// <summary>
    /// Has this player completed the first area
    /// </summary>
    public bool firstLvlCompleted;

    /// <summary>
    /// Has this player completed the second area
    /// </summary>
    public bool secondLvlCompleted;

    /// <summary>
    /// A Mode Jumps made. This means how mane times this player used walking navigation
    /// </summary>
    public int AModeJumps;

    /// <summary>
    /// A Mode Jumps made. This means how mane times this player used teleportation navigation
    /// </summary>
    public int BModeJumps;

    //--------------------------------------
    // Methods
    //--------------------------------------
    void Start ()
    {
        sessionInfo = System.DateTime.Now.ToString("yyy/MM/dd HH:mm:ss");
    }
    /// <summary>
    /// Send the session information to the google form
    /// </summary>
    public void send()
    {
        finalSend = true;
        if (photonView.isMine)
        {
            StartCoroutine(PostInfo());
        }
    }

    /// <summary>
    /// Coroutine used to send the information
    /// </summary>
    /// <returns></returns>
    IEnumerator PostInfo()
    {
        WWWForm wForm = new WWWForm();
        wForm.AddField("entry.1257194160", sessionInfo);
        wForm.AddField("entry.1273438111", device);
        wForm.AddField("entry.1967444367", sessionLength);
        wForm.AddField("entry.1256871862", firstLvlCompleted + "");
        wForm.AddField("entry.1952253521", secondLvlCompleted + "");
        wForm.AddField("entry.1992594368", AModeJumps + "");
        wForm.AddField("entry.962583465", BModeJumps + "");
        byte[] data = wForm.data;
        WWW urlPag = new WWW(URL, data);
        yield return urlPag;
    }


    // Update is called once per frame
    void Update ()
    {
        if(Time.time > 1800f && finalSend == false )
        {
            send();
            finalSend = true;
        }
        if(Input.GetKeyDown(KeyCode.F10))
        {
            finalSend = true;
            send();
        }
	}

    /// <summary>
    /// If the game crashes or stops, send the information 
    /// </summary>
    private void OnApplicationPause()
    {
        if(finalSend == false)
        {
            send();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
