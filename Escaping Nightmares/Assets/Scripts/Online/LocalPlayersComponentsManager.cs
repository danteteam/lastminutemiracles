﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Class that manages the main components of a local player. This helps disable the other players main components so I don't use the components of other player
/// </summary>
public class LocalPlayersComponentsManager : Photon.PunBehaviour
{
    //------------------------------------------------------
    // Public Variables
    //------------------------------------------------------

    /// <summary>
    /// Main Cameras of this player
    /// </summary>
    public Camera[] mainCameras;

    /// <summary>
    /// Main movement and interactions scripts for the local player
    /// </summary>
    public MonoBehaviour[] scripts;

    /// <summary>
    /// Audio Listener for the local player
    /// </summary>
    public AudioListener audioListener;

    /// <summary>
    /// Personal Game Objects to activate
    /// </summary>
    public GameObject[] personalGameObjects;

    /// <summary>
    /// List of possible pedestals close to the local player
    /// </summary>
    public GameObject[] objectsToActivate;

    //--------------------------------------------------------
    // Methods
    //--------------------------------------------------------

	// Use this for initialization
	void Start ()
    {
        if(GameObject.Find("IsTesting") == null)
        {
            bool isLocalPlayer = photonView.isMine;
            if (mainCameras != null)
            {
                foreach (Camera temp in mainCameras)
                {
                    temp.enabled = isLocalPlayer;
                }
            }

            if (scripts != null)
            {
                foreach (MonoBehaviour temp in scripts)
                {
                    if (temp != null)
                    {
                        temp.enabled = isLocalPlayer;
                    }
                }
            }

            if (personalGameObjects != null)
            {
                foreach (GameObject temp in personalGameObjects)
                {
                    if (temp != null)
                    {
                        temp.SetActive(isLocalPlayer);
                    }
                }
            }

            if (audioListener != null)
            {
                audioListener.enabled = isLocalPlayer;
            }

            if (objectsToActivate != null)
            {
                foreach (GameObject temp in objectsToActivate)
                {
                    temp.SetActive(true);
                }
            }
        }        
	}
}
