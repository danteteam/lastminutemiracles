﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINetworkStatus : MonoBehaviour
{
    //---------------------------------------------
    // Public Variables
    //--------------------------------------------

    [Tooltip("Camera that will show the connection process")]
    public GameObject temporalCamera;

    [Tooltip("Array of player texts names that will say if is online or not")]
    public Text[] pTexts;

    [Tooltip("ID of the current Player")]
    public Text playerIDText;

    [Tooltip("Text that says the current connection status")]
    public Text connectionStatusText;


    //---------------------------------------------
    // Private Variables
    //--------------------------------------------

    [Tooltip("Id ")]
    private int currentPlayer;

    //---------------------------------------------
    // Methods
    //--------------------------------------------
    /// <summary>
    /// Message that shows while the player is connecting to the session
    /// </summary>
    public void connectUI()
    {
        connectionStatusText.text = "Connecting...";
    }

    /// <summary>
    /// This shows that the player has or is disconnected
    /// </summary>
    public void disconnectUI()
    {
        connectionStatusText.text = "...";
        pTexts[currentPlayer].text = "Offline";
        currentPlayer = -1;
    }

    /// <summary>
    /// Method called when the player has connected to the server
    /// </summary>
    /// <param name="isHost"></param>
    public void JoinedRoomUI(bool isHost)
    {
        connectionStatusText.text = "Connected to the server";
        temporalCamera.SetActive(false);
    }

    /// <summary>
    /// Sets the player name and show it in the UI Mural
    /// </summary>
    /// <param name="newName"></param>
    public void setPlayerName(string newName)
    {
        char[] charsName = newName.ToCharArray();
        string currentPlayerString = charsName[newName.Length-1]+"";
        currentPlayer = Int32.Parse(currentPlayerString);
       // Debug.Log("Curren Player is: " + currentPlayer);
        if(playerIDText != null)
        {
            playerIDText.text = newName;
        }
    }

    /// <summary>
    /// Display the player connection status in the tutorial murals
    /// </summary>
    /// <param name="index"></param>
    /// <param name="pStatus"></param>
    public void setPlayerStatusTexts(int index, string pStatus)
    {
        if(pTexts[index] != null)
        {
            pTexts[index].text = pStatus;
        }
    }
}
