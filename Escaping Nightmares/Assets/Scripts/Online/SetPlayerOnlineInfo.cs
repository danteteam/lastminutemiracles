﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Sets the player online information
/// </summary>
public class SetPlayerOnlineInfo : Photon.PunBehaviour
{
    //-------------------------------------------------
    // Public Variables
    //-------------------------------------------------

    /// <summary>
    /// Is this Game Object a Camera?
    /// </summary>
    public bool isCamera;

    //--------------------------------------------------
    // Methods
    //--------------------------------------------------

	// Use this for initialization
	void Awake ()
    {
        if(GameObject.Find("IsTesting") == null)
        {
            string newName = photonView.owner.NickName;
            if (isCamera)
            {
                string[] nameArray = newName.Split(' ');
                newName = "Camera " + nameArray[1];
            }
            this.gameObject.name = newName;
        }        
    }
}
